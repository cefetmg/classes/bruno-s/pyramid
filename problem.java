import java.util.List;
import java.util.ArrayList;

import java.util.Map;
import java.util.HashMap;

import java.util.Objects;
import java.util.Scanner;
import java.lang.Math;

interface Graph<T> {

    public List<T> getNeighbours(T u);    

    public Long getCost(T u);
}

class Vertex {

    private final int level;
    private final int index;
    private final long number;

    public Vertex(int level, int index, long number) {
        this.level = level;
        this.index = index;
        this.number = number;
    }

    public int getLevel() { return level; }

    public int getIndex() { return index; }

    public long getNumber() { return number; }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Vertex)) return false;
        Vertex other = (Vertex) o;
        return Objects.equals(this.level, other.level) &&
            Objects.equals(this.index, other.index) &&
            Objects.equals(this.number, other.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(level, index, number);
    }

    @Override
    public String toString() {
        return "[" + level + "," + index + "," + number + "]";
    }
}

class PyramidGraph implements Graph<Vertex> {

    private List<Vertex> vertexes;

    public PyramidGraph(List<Vertex> vertexes) {
        this.vertexes = vertexes;
    }

    public List<Vertex> getNeighbours(Vertex u) {
        int firstChild = u.getLevel() + 1 + u.getIndex();
        int secondChild = u.getLevel() + 1 + u.getIndex() + 1;
        List<Vertex> result = new ArrayList<>();
        if (firstChild < vertexes.size()) {
            result.add(vertexes.get(firstChild));
        }
        if (secondChild < vertexes.size()) {
            result.add(vertexes.get(secondChild));
        }
        return result;
    }

    public Long getCost(Vertex u) {
        return u.getNumber();
    }
}

class SearchUtility {

    private static <T> long maximumPath(Graph<T> graph, T currentValue, Map<T, Long> maximumPathFromVertexToCost) {
        Long maxCost = 0L;
        
        for (T neighbour : graph.getNeighbours(currentValue)) {
            Long result;
            
            if (!maximumPathFromVertexToCost.containsKey(neighbour)) {
                result = maximumPath(graph, neighbour, maximumPathFromVertexToCost);
            } else {
                result = maximumPathFromVertexToCost.get(neighbour);
            }

            maxCost = Math.max(maxCost, result);
        }

        maxCost = graph.getCost(currentValue) + maxCost;
        maximumPathFromVertexToCost.put(currentValue, maxCost);
        
        return maxCost;
    }

    public static <T> long maximumPath(Graph<T> graph, T initialVertex) {
        final Map<T, Long> maximumPathFromVertexToCost = new HashMap<>();
        
        return maximumPath(graph, initialVertex, maximumPathFromVertexToCost);
    }

}

class Main {

    public static void main(String args[]) {
        List<Vertex> vertexes = new ArrayList<>();
        int currentIndex = 0;
        int currentLevel = 0;
        int rep = 0;

        Scanner input = new Scanner(System.in);

        while (input.hasNext()) {
            vertexes.add(new Vertex(currentLevel, currentIndex, input.nextLong()));
            currentIndex++;
            rep++;
            if (rep == currentLevel + 1) {
                currentLevel++;
                rep = 0;
            }
        }

        input.close();

        PyramidGraph pg = new PyramidGraph(vertexes);

        System.out.println(SearchUtility.maximumPath(pg, vertexes.get(0)));
    }
}